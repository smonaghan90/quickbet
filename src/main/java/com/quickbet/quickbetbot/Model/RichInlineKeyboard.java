package com.quickbet.quickbetbot.Model;

import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;

/**
 * Created by Micheli on 22/06/2017.
 */
public class RichInlineKeyboard {
    private String title;
    private InlineKeyboardMarkup keyboard;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public InlineKeyboardMarkup getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(InlineKeyboardMarkup keyboard) {
        this.keyboard = keyboard;
    }
}
