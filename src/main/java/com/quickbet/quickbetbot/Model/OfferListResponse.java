package com.quickbet.quickbetbot.Model;

import com.google.gson.annotations.SerializedName;

public class OfferListResponse extends ListResponse {
    @SerializedName("available-amount")
    public double availableAmount;

    public String currency;
    public Offer[] offers;
}
