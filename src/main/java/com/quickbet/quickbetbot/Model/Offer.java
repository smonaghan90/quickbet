package com.quickbet.quickbetbot.Model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Offer {
    public long id;

    @SerializedName("event-id")
    public long eventId;

    @SerializedName("event-name")
    public String eventName;

    @SerializedName("market-id")
    public long marketId;

    @SerializedName("market-name")
    public String marketName;

    @SerializedName("runner-name")
    public String runnerName;

    public String side;
    public double odds;

    @SerializedName("odds-type")
    public String oddsType;

    public String currency;
    public String status;

    public double stake;
    public double remaining;

    public double commission;

    @SerializedName("created-at")
    public String createdAt;

    @SerializedName("potential-profit")
    public double potentialProfit;

    @SerializedName("remaining-potential-profit")
    public double remainingPotentialProfit;

    @SerializedName("potential-liability")
    public double potentialLiability;

    @SerializedName("remaining-potential-liability")
    public double remainingPotentialLiability;

    public volatile boolean edge;

    public double getPotentialProfit() {
        return "back".equals(side) ?
                potentialProfit - commission :
                stake - commission;
    }

    public boolean isMatched() {
        return "matched".equals(status);
    }

    public double getMatchedAmount() {
        return stake - remaining;
    }
}
