package com.quickbet.quickbetbot.Model;

import java.util.List;

public class ListResponse {

    public int total;
    private int offset;

    public boolean hasMoreItems(List<?> returnedItems) {
        int returnedItemCount = returnedItems != null ? returnedItems.size() : 0;
        return offset + returnedItemCount < total;
    }

}
