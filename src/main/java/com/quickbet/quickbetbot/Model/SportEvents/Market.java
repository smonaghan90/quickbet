package com.quickbet.quickbetbot.Model.SportEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Market {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("runners")
    @Expose
    private List<Runner> runners = new ArrayList<>();
    @SerializedName("start")
    @Expose
    private Date start;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("market-type")
    @Expose
    private String marketType;
    @SerializedName("event-id")
    @Expose
    private Long eventId;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("grading-type")
    @Expose
    private String gradingType;
    @SerializedName("in-running-flag")
    @Expose
    private Boolean inRunningFlag;
    @SerializedName("allow-live-betting")
    @Expose
    private Boolean allowLiveBetting;
    @Expose
    private String handicap;
    @SerializedName("asian-handicap")
    @Expose
    private String asianHandicap;
    @SerializedName("volume")
    @Expose
    private Double volume;
    @SerializedName("back-overround")
    @Expose
    private Double backOverRound;
    @SerializedName("lay-overround")
    @Expose
    private Double layOverRound;
    private boolean isEdge;
    //Utils
    private Event event;
    private String sportRelated;


    /**
     * @return The marketType
     */
    public String getMarketType() {
        return marketType;
    }

    public void setMarketType(String marketType) {
        this.marketType = marketType;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The runners
     */
    public List<Runner> getRunners() {
        return runners;
    }

    /**
     * @param runners The runners
     */
    public void setRunners(List<Runner> runners) {
        this.runners = runners;
    }

    /**
     * @return The start
     */
    public Date getStart() {
        return start;
    }

    /**
     * @param start The start
     */
    public void setStart(Date start) {
        this.start = start;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The eventId
     */
    public Long getEventId() {
        return eventId;
    }

    /**
     * @param eventId The event-id
     */
    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    /**
     * @return The id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The gradingType
     */
    public String getGradingType() {
        String value;
        if (gradingType == null) {
            setGradingType(getMarketType());
            value = getMarketType();
        } else {
            value = gradingType;
        }
        return value;
    }

    /**
     * @param gradingType The grading-type
     */
    public void setGradingType(String gradingType) {
        this.gradingType = gradingType;
    }

    /**
     * @return The inRunningFlag
     */
    public Boolean getInRunningFlag() {
        return inRunningFlag;
    }

    /**
     * @param inRunningFlag The in-running-flag
     */
    public void setInRunningFlag(Boolean inRunningFlag) {
        this.inRunningFlag = inRunningFlag;
    }

    /**
     * @return The allowLiveBetting
     */
    public Boolean getAllowLiveBetting() {
        return allowLiveBetting;
    }

    /**
     * @param allowLiveBetting The allow-live-betting
     */
    public void setAllowLiveBetting(Boolean allowLiveBetting) {
        this.allowLiveBetting = allowLiveBetting;
    }

    public String getHandicap() {
        return handicap;
    }

    public void setHandicap(String handicap) {
        this.handicap = handicap;
    }

    public String getAsianHandicap() {
        return asianHandicap;
    }

    public void setAsianHandicap(String asianHandicap) {
        this.asianHandicap = asianHandicap;
    }

    public boolean isEdge() {
        return isEdge;
    }

    public void setEdge(boolean edge) {
        isEdge = edge;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getBackOverRound() {
        return backOverRound;
    }

    public void setBackOverRound(Double backOverRound) {
        this.backOverRound = backOverRound;
    }

    public Double getLayOverRound() {
        return layOverRound;
    }

    public void setLayOverRound(Double layOverRound) {
        this.layOverRound = layOverRound;
    }


    //Utils


    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getSportRelated() {
        return sportRelated;
    }

    public void setSportRelated(String sportRelated) {
        this.sportRelated = sportRelated;
    }
}
