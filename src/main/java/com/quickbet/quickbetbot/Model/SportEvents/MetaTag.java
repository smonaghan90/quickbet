package com.quickbet.quickbetbot.Model.SportEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MetaTag {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("url-name")
    @Expose
    private String urlName;

    /**
     * @return The id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The urlName
     */
    public String getUrlName() {
        return urlName;
    }

    /**
     * @param urlName The url-name
     */
    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

}
