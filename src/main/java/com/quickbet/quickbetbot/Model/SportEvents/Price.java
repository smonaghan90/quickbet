package com.quickbet.quickbetbot.Model.SportEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("odds")
    @Expose
    private Double odds;
    @SerializedName("side")
    @Expose
    private String side;
    @SerializedName("available-amount")
    @Expose
    private Double availableAmount;
    @SerializedName("odds-type")
    @Expose
    private String oddsType;
    @SerializedName("decimal-odds")
    @Expose
    private Double decimalOdds;
    @SerializedName("exchange-type")
    @Expose
    private String exchangeType;

    /**
     * @return The currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency The currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return The odds
     */
    public Double getOdds() {
        return odds;
    }

    /**
     * @param odds The odds
     */
    public void setOdds(Double odds) {
        this.odds = odds;
    }

    /**
     * @return The side
     */
    public String getSide() {
        return side;
    }

    /**
     * @param side The side
     */
    public void setSide(String side) {
        this.side = side;
    }

    /**
     * @return The availableAmount
     */
    public Double getAvailableAmount() {
        return availableAmount;
    }

    /**
     * @param availableAmount The available-amount
     */
    public void setAvailableAmount(Double availableAmount) {
        this.availableAmount = availableAmount;
    }

    /**
     * @return The oddsType
     */
    public String getOddsType() {
        return oddsType;
    }

    /**
     * @param oddsType The odds-type
     */
    public void setOddsType(String oddsType) {
        this.oddsType = oddsType;
    }

    /**
     * @return The decimalOdds
     */
    public Double getDecimalOdds() {
        return decimalOdds;
    }

    /**
     * @param decimalOdds The decimal-odds
     */
    public void setDecimalOdds(Double decimalOdds) {
        this.decimalOdds = decimalOdds;
    }

    /**
     * @return The exchangeType
     */
    public String getExchangeType() {
        return exchangeType;
    }

    /**
     * @param exchangeType The exchange-type
     */
    public void setExchangeType(String exchangeType) {
        this.exchangeType = exchangeType;
    }

}
