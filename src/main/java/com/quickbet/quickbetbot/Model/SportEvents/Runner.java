package com.quickbet.quickbetbot.Model.SportEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("prices")
    @Expose
    private List<Price> prices = new ArrayList<>();
    @SerializedName("event-id")
    @Expose
    private Long eventId;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("market-id")
    @Expose
    private Long marketId;

    //util
    private Market market;
    private Event event;
    private boolean hasHeader;
    private boolean hasFooter;
    private boolean hasEventName;


    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The prices
     */
    public List<Price> getPrices() {
        return prices;
    }

    /**
     * @param prices The prices
     */
    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    /**
     * @return The eventId
     */
    public Long getEventId() {
        return eventId;
    }

    /**
     * @param eventId The event-id
     */
    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    /**
     * @return The id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The marketId
     */
    public Long getMarketId() {
        return marketId;
    }

    /**
     * @param marketId The market-id
     */
    public void setMarketId(Long marketId) {
        this.marketId = marketId;
    }

    //UTILS


    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public boolean hasHeader() {
        return hasHeader;
    }

    public void setHasHeader(boolean hasHeader) {
        this.hasHeader = hasHeader;
    }

    public boolean hasFooter() {
        return hasFooter;
    }

    public void setHasFooter(boolean hasFooter) {
        this.hasFooter = hasFooter;
    }

    public boolean hasEventName() {
        return hasEventName;
    }

    public void setHasEventName(boolean hasEventName) {
        this.hasEventName = hasEventName;
    }
}
