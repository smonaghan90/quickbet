package com.quickbet.quickbetbot.Model.SportEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SportEventsForBetting {

    @SerializedName("offset")
    @Expose
    private Long offset;
    @SerializedName("total")
    @Expose
    private Long total;
    @SerializedName("per-page")
    @Expose
    private Long perPage;
    @SerializedName("events")
    @Expose
    private List<Event> events = new ArrayList<>();

    /**
     * @return The offset
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * @param offset The offset
     */
    public void setOffset(Long offset) {
        this.offset = offset;
    }

    /**
     * @return The total
     */
    public Long getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    public void setTotal(Long total) {
        this.total = total;
    }

    /**
     * @return The perPage
     */
    public Long getPerPage() {
        return perPage;
    }

    /**
     * @param perPage The per-page
     */
    public void setPerPage(Long perPage) {
        this.perPage = perPage;
    }

    /**
     * @return The events
     */
    public List<Event> getEvents() {
        return events;
    }

    /**
     * @param events The events
     */
    public void setEvents(List<Event> events) {
        this.events = events;
    }

}
