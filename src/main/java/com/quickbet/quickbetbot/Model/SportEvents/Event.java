package com.quickbet.quickbetbot.Model.SportEvents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Event {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("start")
    @Expose
    private Date start;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("sport-id")
    @Expose
    private Long sportId;
    @SerializedName("category-id")
    @Expose
    private List<Long> categoryId = new ArrayList<>();
    @SerializedName("in-running-flag")
    @Expose
    private Boolean inRunningFlag;
    @SerializedName("allow-live-betting")
    @Expose
    private Boolean allowLiveBetting;
    @SerializedName("markets")
    @Expose
    private List<Market> markets = new ArrayList<>();
    @SerializedName("meta-tags")
    @Expose
    private List<MetaTag> metaTags = new ArrayList<>();
    @Expose
    private Double volume;

    private boolean isEdge;

    /**
     * @return The id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The start
     */
    public Date getStart() {
        return start;
    }

    /**
     * @param start The start
     */
    public void setStart(Date start) {
        this.start = start;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The sportId
     */
    public Long getSportId() {
        return sportId;
    }

    /**
     * @param sportId The sport-id
     */
    public void setSportId(Long sportId) {
        this.sportId = sportId;
    }

    /**
     * @return The categoryId
     */
    public List<Long> getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId The category-id
     */
    public void setCategoryId(List<Long> categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return The inRunningFlag
     */
    public Boolean getInRunningFlag() {
        return inRunningFlag;
    }

    /**
     * @param inRunningFlag The in-running-flag
     */
    public void setInRunningFlag(Boolean inRunningFlag) {
        this.inRunningFlag = inRunningFlag;
    }

    /**
     * @return The allowLiveBetting
     */
    public Boolean getAllowLiveBetting() {
        return allowLiveBetting;
    }

    /**
     * @param allowLiveBetting The allow-live-betting
     */
    public void setAllowLiveBetting(Boolean allowLiveBetting) {
        this.allowLiveBetting = allowLiveBetting;
    }

    /**
     * @return The markets
     */
    public List<Market> getMarkets() {
        return markets;
    }

    /**
     * @param markets The markets
     */
    public void setMarkets(List<Market> markets) {
        this.markets = markets;
    }

    /**
     * @return The metaTags
     */
    public List<MetaTag> getMetaTags() {
        return metaTags;
    }

    /**
     * @param metaTags The meta-tags
     */
    public void setMetaTags(List<MetaTag> metaTags) {
        this.metaTags = metaTags;
    }

    public boolean isEdge() {
        return isEdge;
    }

    public void setEdge(boolean edge) {
        isEdge = edge;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }
}
