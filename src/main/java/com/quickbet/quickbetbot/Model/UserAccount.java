package com.quickbet.quickbetbot.Model;


import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UserAccount {
    public String username;
    public double balance;
    public double exposure;
    public String currency;

    @SerializedName("confirm-tutorial")
    public boolean confirmTutorial;

    @SerializedName("confirm-terms")
    public boolean confirmTerms;

    @SerializedName("free-funds")
    public double freeFunds;

    @SerializedName("odds-type")
    public String oddsType;

    @SerializedName("last-login")
    public Date lastLogin;

    @SerializedName("bet-confirmation")
    public boolean betConfirmation;

    @SerializedName("user-id")
    public int userId;
    @SerializedName("exchange-type")
    public String exchangeType;


}
