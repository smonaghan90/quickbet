package com.quickbet.quickbetbot.Model;


import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UserSession {
    @SerializedName("session-token")
    public String sessionToken;

    public UserAccount account;

    @SerializedName("last-login")
    public Date lastLogin;
}
