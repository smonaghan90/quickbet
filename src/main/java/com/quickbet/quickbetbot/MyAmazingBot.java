package com.quickbet.quickbetbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.quickbet.quickbetbot.DBUtils.DBHelper;
import com.quickbet.quickbetbot.Model.Offer;
import com.quickbet.quickbetbot.Model.OfferListResponse;
import com.quickbet.quickbetbot.Model.SportEvents.Event;
import com.quickbet.quickbetbot.Model.SportEvents.Price;
import com.quickbet.quickbetbot.Model.SportEvents.Runner;
import com.quickbet.quickbetbot.Model.SportEvents.SportEventsForBetting;
import com.quickbet.quickbetbot.Model.UserSession;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.*;

public class MyAmazingBot extends TelegramLongPollingBot {


    private static final String API_ENDPOINT = "https://matchbook.com/rest/bpapi";
    private static final String BALANCE_ENDPOINT = "https://matchbook.com/edge/rest/account/balance/";
    private static final String LOGIN_ENDPOINT = "https://matchbook.com/bpapi/rest/security/session/";
    private static final String GAMES_ENDPOINT = "https://matchbook.com/beta/api/search?language=en&types=event&limit=500&offset=0&query=";
    private static final String ALL_EVENTS_ENDPOINT = "https://matchbook.com/edge/rest/events/$id?include-markets=true&include-runners=true&include-prices=true&per-page=500";
    private static final String POPULAR_EVENTS = "https://matchbook.com/edge/rest/popular-markets?old-format=true&per-page=500";
    private static final String OFFER_URL = "https://matchbook.com/edge/rest/offers";
    private static final String START = "/start";
    private static final String LOGIN = "/login";
    private static final String REVOKE_AUTH = "/unsubscribe";
    private static final String HELP = "/help";
    //TODO: no search
    private static final String SHOW_GAMES_4_SOCCER = "/showGames soccer";
    private static final String SHOW_GAMES_4_FOOTBALL = "/showGames football";
    private static final String SHOW_GAMES_4_TENNIS = "/showGames tennis";
    private static final String SHOW_GAMES_4_HR = "/showGames horse racing";
    private static final String SHOW_BALANCE = "/showBalance";
    private static final String SHOW_POPULAR = "/showPopular";
    //TODO: search

    public static Map<Long, String> userSessionMap = new HashMap<>();

    private String sport = "soccer";
    private String code = "SC";
    private String token = "444469449:AAFPFWAcPYfid5IF0MuxYNgtKo4vpwaXxm8";////"417892541:AAEc7o3n8WgoAygX5SmT-fwMSwkJM-xY6UA";
    //private String token = "417892541:AAEc7o3n8WgoAygX5SmT-fwMSwkJM-xY6UA";


    public String getBotUsername() {
        return "QuickBet";
    }

    public void onUpdateReceived(Update update) {

        if (update.hasMessage() && update.getMessage().hasText()) {
            DBHelper.connect();
            Long chatID = update.getMessage().getChatId();
            String text = update.getMessage().getText();
            SendMessage message = new SendMessage().setChatId(chatID);
            //check if command:
            if (text.startsWith("/")) {
                Integer userId = update.getMessage().getFrom().getId();
                //Check which command:
                if (text.startsWith(START)) {
                    message.setText("Welcome to Matchbook Telegram Bot!");
                } else if (text.startsWith(LOGIN)) {
                    String pin = text.substring(text.indexOf(" "));
                    message.setText(login(pin) + "\n" + showbalance(userId));
                } else if (text.startsWith(REVOKE_AUTH)) {
                    String pin = text.substring(text.indexOf(" "));
                    message.setText(unsubscribe(pin));
                } else if (text.startsWith(HELP)) {
                    message.setText(help());
                } else if (text.startsWith(SHOW_GAMES_4_SOCCER)) {
                    InlineKeyboardMarkup keyboardMarkupSoccer = showGames(userId, "soccer");
                    if (keyboardMarkupSoccer != null) {
                        message.setReplyMarkup(keyboardMarkupSoccer);
                        message.setText(SHOW_GAMES_4_SOCCER);
                    } else {
                        message.setText("Please login");
                    }
                } else if (text.startsWith(SHOW_GAMES_4_FOOTBALL)) {
                    InlineKeyboardMarkup keyboardMarkupFootball = showGames(userId, "football");
                    if (keyboardMarkupFootball != null) {
                        message.setReplyMarkup(keyboardMarkupFootball);
                        message.setText(SHOW_GAMES_4_FOOTBALL);
                    } else {
                        message.setText("Please login");
                    }
                } else if (text.startsWith(SHOW_GAMES_4_TENNIS)) {
                    InlineKeyboardMarkup keyboardMarkupTennis = showGames(userId, "tennis");
                    if (keyboardMarkupTennis != null) {
                        message.setReplyMarkup(keyboardMarkupTennis);
                        message.setText(SHOW_GAMES_4_TENNIS);
                    } else {
                        message.setText("Please login");
                    }
                } else if (text.startsWith(SHOW_GAMES_4_HR)) {
                    InlineKeyboardMarkup keyboardMarkupHR = showGames(userId, "horse-racing");
                    if (keyboardMarkupHR != null) {
                        message.setReplyMarkup(keyboardMarkupHR);
                        message.setText(SHOW_GAMES_4_HR);
                    } else {
                        message.setText("Please login");
                    }
                } else if (text.startsWith(SHOW_BALANCE)) {
                    message.setText(showbalance(userId));
                } else if (text.startsWith(SHOW_POPULAR)) {
                    InlineKeyboardMarkup popularKeyboardMarkup = showPopularGames(userId);
                    if (popularKeyboardMarkup != null) {
                        message.setReplyMarkup(popularKeyboardMarkup);
                        message.setText(SHOW_POPULAR);
                    } else {
                        message.setText("Please login");
                    }
                } else {
                    message.setText("Unknown command");
                }
            }
            try {
                sendMessage(message); // Sending our message object to user
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        } else {
            if (update.hasCallbackQuery()) {
                // Set variables
                String call_data = update.getCallbackQuery().getData();
                long message_id = update.getCallbackQuery().getMessage().getMessageId();
                long chat_id = update.getCallbackQuery().getMessage().getChatId();
                Integer userId = update.getMessage().getFrom().getId();
                if (call_data.startsWith("empty")) {
                    //do nothing
                }
                if (call_data.startsWith("Prices_")) {
                    String id = call_data.substring(7);
                    SendMessage price_message = new SendMessage()
                            .setChatId(chat_id);
                    InlineKeyboardMarkup keyboardMarkup = showPrice(userId, id);
                    if (keyboardMarkup != null) {
                        price_message.setReplyMarkup(keyboardMarkup);
                        price_message.setText(retrieveEventMarketName(userId, id));
                    } else {
                        price_message.setText("Please login");
                    }

                    try {
                        sendMessage(price_message);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                if (call_data.startsWith("Back_")) {
                    String runnerId = call_data.substring(5, call_data.lastIndexOf("_"));
                    String eventId = call_data.substring(call_data.lastIndexOf("_") + 1);
                    SendMessage message = new SendMessage()
                            .setChatId(chat_id);
                    InlineKeyboardMarkup keyboardMarkup = showStakeKeyboard(userId, eventId, runnerId, "back");
                    if (keyboardMarkup != null) {
                        message.setReplyMarkup(keyboardMarkup);
                        Runner r = retrieveRunnerInfoFromId(userId, eventId, runnerId);
                        message.setText(r.getName() + " " + getBestBackForRunner(r, true));
                    } else {
                        message.setText("Please login");
                    }
                    try {
                        sendMessage(message);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                if (call_data.startsWith("Lay_")) {
                    String runnerId = call_data.substring(4, call_data.lastIndexOf("_"));
                    String eventId = call_data.substring(call_data.lastIndexOf("_") + 1);
                    SendMessage message = new SendMessage()
                            .setChatId(chat_id);
                    InlineKeyboardMarkup keyboardMarkup = showStakeKeyboard(userId, eventId, runnerId, "lay");
                    if (keyboardMarkup != null) {
                        message.setReplyMarkup(keyboardMarkup);
                        Runner r = retrieveRunnerInfoFromId(userId, eventId, runnerId);
                        message.setText(r.getName() + " " + getBestLayForRunner(r, true));
                    } else {
                        message.setText("Please login");
                    }
                    try {
                        sendMessage(message);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                if (call_data.startsWith("Amount_")) {
                    String value = call_data.substring(7, call_data.lastIndexOf("_"));
                    String id = call_data.substring(call_data.lastIndexOf("_") + 1, call_data.lastIndexOf("-"));
                    String side = call_data.substring(call_data.lastIndexOf("-") + 1, call_data.lastIndexOf("$"));
                    String eventId = call_data.substring(call_data.lastIndexOf("$") + 1);
                    Runner r = retrieveRunnerInfoFromId(userId, eventId, id);
                    String response = placeOfferTask(userId, value, r, side);
                    SendMessage message = new SendMessage()
                            .setChatId(chat_id);
                    message.setText(response);
                    try {
                        sendMessage(message);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


    }

    private Runner retrieveRunnerInfoFromId(Integer userId, String eventId, String runnerId) {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(ALL_EVENTS_ENDPOINT.replace("$id", eventId));
            // add header
            get.setHeader("content-type", "application/json");
            get.setHeader("accept", "application/json");
            get.setHeader("session-token", getToken(userId));
            HttpResponse resp = client.execute(get);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");
            Gson gson = new GsonBuilder().create();
            try {
                Runner runner = new Runner();
                Event event = gson.fromJson(json, Event.class);
                for (Runner r : event.getMarkets().get(0).getRunners()) {
                    if (String.valueOf(r.getId()).equals(runnerId)) {
                        runner = r;
                        break;
                    }
                }
                return runner;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String retrieveEventMarketName(Integer userId, String id) {
        try {

            HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(ALL_EVENTS_ENDPOINT.replace("$id", id));
            // add header
            get.setHeader("content-type", "application/json");
            get.setHeader("accept", "application/json");
            get.setHeader("session-token", getToken(userId));

            HttpResponse resp = client.execute(get);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");

            Gson gson = new GsonBuilder().create();
            try {
                Event e = gson.fromJson(json, Event.class);
                String title = e.getName() + " " + e.getMarkets().get(0).getName();
                return title;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Event not found!";
    }

    private InlineKeyboardMarkup showStakeKeyboard(Integer userId, String eventId, String id, String side) {
        if (getToken(userId) == null) {
            return null;
        }
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        for (int i = 5; i <= 25; i = i + 5) {
            rowInline.add(new InlineKeyboardButton().setText(String.valueOf(i)).setCallbackData("Amount_" + String.valueOf(i) + "_" + id + "-" + side + "$" + eventId));
        }
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    private InlineKeyboardMarkup showPopularGames(Integer userId) {
        if (getToken(userId) == null) {
            return null;
        }
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(POPULAR_EVENTS);

            // add header
            get.setHeader("content-type", "application/json");
            get.setHeader("accept", "application/json");
            get.setHeader("session-token", getToken(userId));

            HttpResponse resp = client.execute(get);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");

            Gson gson = new GsonBuilder().create();
            try {
                SportEventsForBetting sportEventsForBetting = gson.fromJson(json, SportEventsForBetting.class);
                InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                for (Event e : sportEventsForBetting.getEvents()) {
                    List<InlineKeyboardButton> rowInline = new ArrayList<>();
                    rowInline.add(new InlineKeyboardButton().setText(e.getName()).setCallbackData("Prices_" + e.getId().toString()));
                    rowsInline.add(rowInline);

                }
                markupInline.setKeyboard(rowsInline);
                return markupInline;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private InlineKeyboardMarkup showPrice(Integer userId, String id) {
        if (getToken(userId) == null) {
            return null;
        }
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(ALL_EVENTS_ENDPOINT.replace("$id", id));

            // add header
            get.setHeader("content-type", "application/json");
            get.setHeader("accept", "application/json");
            get.setHeader("session-token", getToken(userId));

            HttpResponse resp = client.execute(get);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");
            Gson gson = new GsonBuilder().create();
            try {
                Event event = gson.fromJson(json, Event.class);
                InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                for (Runner r : event.getMarkets().get(0).getRunners()) {
                    List<InlineKeyboardButton> rowInline = new ArrayList<>();
                    rowInline.add(new InlineKeyboardButton().setText(r.getName()).setCallbackData("empty"));
                    rowInline.add(new InlineKeyboardButton().setText(getBestBackForRunner(r, true)).setCallbackData("Back_" + r.getId() + "_" + event.getId()));
                    rowInline.add(new InlineKeyboardButton().setText(getBestLayForRunner(r, true)).setCallbackData("Lay_" + r.getId() + "_" + event.getId()));
                    rowsInline.add(rowInline);

                }
                markupInline.setKeyboard(rowsInline);

                return markupInline;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    private String showbalance(Integer userId) {
        if (getToken(userId) == null) {
            return "Please login";
        }
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(BALANCE_ENDPOINT);

            // add header
            get.setHeader("content-type", "application/json");
            get.setHeader("accept", "application/json");
            get.setHeader("session-token", getToken(userId));

            HttpResponse resp = client.execute(get);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");

            JSONParser parser = new JSONParser();
            Object resultObject = parser.parse(json);


            JSONObject obj = (JSONObject) resultObject;
            Double balance = (Double) obj.get("balance");
            Double freeFunds = (Double) obj.get("free-funds");

            return "Your balance is: " + formatMoneyDecimal(balance) + " & your available amount is: " + formatMoneyDecimal(freeFunds);


        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "Failed to retrieve balance";


    }

    private InlineKeyboardMarkup showGames(Integer userId, String sport) {
        if (getToken(userId) == null) {
            return null;
        }
        try {
            String Output = "";
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(GAMES_ENDPOINT + sport);

            // add header
            get.setHeader("content-type", "application/json");
            get.setHeader("accept", "application/json");
            get.setHeader("session-token", getToken(userId));

            HttpResponse resp = client.execute(get);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");

            JSONParser parser = new JSONParser();
            Object resultObject = parser.parse(json);
            JSONObject obj = (JSONObject) resultObject;
            JSONArray priceResults = (JSONArray) obj.get("results");
            InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
            for (int i = 0; i < priceResults.size(); i++) {
                List<InlineKeyboardButton> rowInline = new ArrayList<>();
                JSONObject home = (JSONObject) priceResults.get(i);
                rowInline.add(new InlineKeyboardButton().setText(home.get("name").toString()).setCallbackData("Prices_" + home.get("id").toString()));
                rowsInline.add(rowInline);
            }
            markupInline.setKeyboard(rowsInline);

            return markupInline;

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    private String help() {
        String outputString = "";
        outputString += "/help \n-> show list of commands \n";
        outputString += "/login [PIN] \n-> Log-in the user \n";
        outputString += "/showBalance \n-> Return amount & free funds of the user \n";
        outputString += "/showPopular \n-> Show popular games list \n";
        outputString += "/showGames soccer \n-> Show soccer events \n";
        outputString += "/showGames horse racing \n-> Show horse racing events \n";
        outputString += "/showGames tennis \n-> Show tennis events \n";
        outputString += "/showGames football \n-> Show football events \n";
        return outputString;
    }

    private String login(String pin) {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(LOGIN_ENDPOINT);

            // add header
            post.setHeader("content-type", "application/json");
            post.setHeader("accept", "application/json");
            post.setHeader("accept", "application/json");
            ResultSet rs = DBHelper.getUsernameAndPwd(pin);
            String username = "";
            String pwd = "";
            if (rs.next()) {
                username = rs.getString("userName");
                pwd = rs.getString("password");
            }
            String user_json = "{ \"username\":\"" + username + "\", \"password\":\"" + pwd + "\" }";
            StringEntity entity = new StringEntity(user_json);
            post.setEntity(entity);

            HttpResponse resp = client.execute(post);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");
            Gson gson = new GsonBuilder().create();
            UserSession us = gson.fromJson(json, UserSession.class);
            DBHelper.setUserToken(us.sessionToken);
            return "Login successfull";

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Failed to login";
    }

    private String unsubscribe(String pin) {
        try {
            DBHelper.removeUser(pin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Failed to login";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    private String getBestBackForRunner(Runner r, boolean isMessage) {
        String output = "";
        for (Price p : r.getPrices()) {
            if (p.getSide().equals("back")) {
                if (isMessage) {
                    output += "BACK @" + p.getOdds() + " ";
                } else {
                    output += p.getOdds();
                }
                break;
            }
        }
        if (output.equals("")) {
            if (isMessage) {
                output = "MAKE OFFER";
            } else {
                output = "0";
            }
        }
        return output;
    }
    

    private String getBestLayForRunner(Runner r, boolean isMessage) {
        String output = "";
        for (Price p : r.getPrices()) {
            if (p.getSide().equals("lay")) {
                if (isMessage) {
                    output += "LAY @" + p.getOdds() + " ";
                } else {
                    output += p.getOdds();
                }
                break;
            }
        }
        if (output.equals("")) {
            if (isMessage) {
                output = "MAKE OFFER";
            } else {
                output = "0";
            }
        }
        return output;
    }

    private Double getAvailableAmount(Runner r){
    	
    	for (Price p : r.getPrices()){
    		return p.getAvailableAmount();
    		
    	}
		return null;
    	
    	
    	
    }
    private String placeOfferTask(Integer userId, String value, Runner r, String side) {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(OFFER_URL);
            // add header
            post.setHeader("content-type", "application/json");
            post.setHeader("accept", "application/json");
            post.setHeader("session-token", getToken(userId));
            JsonObject offer = new JsonObject();
            Double available = getAvailableAmount(r);
            double valueStake = Double.parseDouble(value);
            if (available < valueStake){
            	
            	//Call Brokerage Code 
            	System.out.println("Broker being contacted");
            }
            
            if (side.equals("back")) {
                String back = getBestBackForRunner(r, false);
                if (back.equals("0")) {
                    return "Not supported yet";
                } else {
                    offer.addProperty("odds", back);
                }
            } else {
                String lay = getBestLayForRunner(r, false);
                if (lay.equals("0")) {
                    return "Not supported yet";
                } else {
                    offer.addProperty("odds", lay);
                }
            }
            offer.addProperty("stake", value);
            offer.addProperty("runner-id", r.getId());
            offer.addProperty("side", side);
            JsonArray offers = new JsonArray();
            offers.add(offer);
            // Construct the complete request payload object
            // which consists in single "offers" array property.
            JsonObject data = new JsonObject();
            data.add("offers", offers);
            post.setEntity(new StringEntity(data.toString()));
            HttpResponse resp = client.execute(post);
            String json = EntityUtils.toString(resp.getEntity(), "UTF-8");

            Gson gson = new GsonBuilder().create();
            try {
                OfferListResponse response = gson.fromJson(json, OfferListResponse.class);
                Offer o = response.offers[0];
                if (o.isMatched()) {
                    // Offer matched, display matched amount and odds.
                    return "Bet submitted! Matched " + o.getMatchedAmount() + " at " + o.odds + ". " + showbalance(userId);

                } else {
                    // Offer not matched, display odds only.
                    return "Bet submitted! Unmatched at " + o.odds + ". " + showbalance(userId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "An error occurred...";
    }

    private static final DecimalFormat sMoneyFormatDecimal = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.getDefault());

    public static String formatMoneyDecimal(double value) {
        sMoneyFormatDecimal.setMaximumFractionDigits(2);
        sMoneyFormatDecimal.setMinimumFractionDigits(2);
        return sMoneyFormatDecimal.format(value);
    }

    public String getToken(Integer userId) {
        ResultSet rs = DBHelper.getUserToken(userId);
        String userID = "";
        try {
            if (rs.next()) {
                userID = rs.getString("sessionToken");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userID;
    }
}