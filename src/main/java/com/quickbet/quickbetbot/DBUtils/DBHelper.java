package com.quickbet.quickbetbot.DBUtils;

import java.sql.*;

/**
 * Created by Micheli on 03/07/2017.
 */

public class DBHelper {

    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static Connection connection = null;
    public static Statement statement = null;

    public static void connect() {
        String url = "jdbc:mysql://54.171.252.58:3306/quickbet";
        String username = "qb";
        String password = "quickbetkids$$1";
        try {
            Class.forName(DRIVER).newInstance();
            try {
                connection = DriverManager.getConnection(url, username, password);
                statement = connection.createStatement();
                System.out.println("Connected to DB");
            } catch (SQLException e) {
                System.out.println("SQLException: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("VendorError: " + e.getErrorCode());
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public static ResultSet getUsernameAndPwd(String pin) {
        ResultSet rs = null;
        try {
            rs = statement.executeQuery("SELECT * FROM subscription WHERE pincode = " + pin);
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet getUserToken(Integer userID) {
        ResultSet rs = null;
        try {
            rs = statement.executeQuery("SELECT * FROM subscription WHERE userID = " + userID);
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public static void setUserToken(String token) {
        try {
            String query = "UPDATE subscription SET sessionToken = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, token);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeUser(String pin) {
        try {
            String query = "DELETE FROM subscription WHERE pincode = " + pin;
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
